# VIDEORELAY-ANSIBLE

* Take stream from rtsp, create HLS stream, integrate them in a video player.

## Deploy your videorelay

```sh
pip install ansible                                   # Install ansible
ansible-galaxy collection install community.crypto    # Install dependencies from ansible galaxy
cp -r inventory/dev inventory/prod                    # create and customize your inventory
ansible-playbook site.yml -i inventory/prod/hosts.ini # Run ansible
```
